//Pending things to change on display side:



//#define BAROENABLE

#include <VescUart.h>
#include <EEPROM.h>

#ifdef BAROENABLE
#include <BME280I2C.h>  //For board temp/hum/press sensor
#include <Wire.h>   //i2c
#endif

#include <Vescan.h>
#include <Vescgen.h>
#include <FlexCAN_T4.h>

VescUart UART;

const bool canDebugSerial = false;
static CAN_message_t msg;
int canTestVal = 300;
bool canParsed = false;

//Some colors
const int green = 2016;
const int red = 63488;
const int blue = 1119;
const int turq = 2047;

const float hotTempLim = 43.0;

unsigned int lowCell = 0;
unsigned int highCell = 0;
unsigned int cellDiff = 0;
float packVolts = 0;
float packCurrent = 0;
float packPower = 0;
int64_t packEnergy = 0;
unsigned int cellVolts[15] = {0};
bool cellBalancing[15] = {false};

float externalPower = 0; //This is the difference between BMS total power measurement and VESC power measuremet, ie. solar charger / other load

bool balancing = false;
int bmsExternalTemp = 0;
int bmsTemp = 0;
int bmsChipTempHi = 0;
int bmsChipTempLo = 0;
bool bmsHot = false;
byte chargeInhibited = false;
byte dischargeInhibited = false;

//Update flags, set when relevant can msg is received, screen is updated based on these
bool bmsGenericUpdated = false;
bool bmsCells1_5Updated = false;
bool bmsCells6_10Updated = false;
bool bmsCells11_15Updated = false;
bool bmsRuntimeUpdated = false;
bool bmsEnergyUpdated = false;
bool bmsCellExtremesUpdated = false;
bool bmsTemperatureUpdated = false;
bool bmsCells1_2BalUpdated = false;
bool bmsCells3_4BalUpdated = false;
bool bmsCells5_6BalUpdated = false;
bool bmsCells7_8BalUpdated = false;
bool bmsCells9_10BalUpdated = false;
bool bmsCells11_12BalUpdated = false;
bool bmsCells13_14BalUpdated = false;
bool bmsCells15BalUpdated = false;

#ifdef BAROENABLE
#include <EnvironmentCalculations.h>
BME280I2C::Settings settings(
  BME280::OSR_X16,
  BME280::OSR_X16,
  BME280::OSR_X16,
  BME280::Mode_Forced,
  BME280::StandbyTime_50ms,
  BME280::Filter_16,
  BME280::SpiEnable_False,
  0x76 // I2C address. I2C specific.
);

//BME280I2C::Settings settings(
//  BME280::OSR_X1,
//  BME280::OSR_X1,
//  BME280::OSR_X1,
//  BME280::Mode_Forced,
//  BME280::StandbyTime_50ms,
//  BME280::Filter_Off,
//  BME280::SpiEnable_False,
//  0x76 // I2C address. I2C specific.
//);


BME280I2C bme(settings);    // Default : forced mode, standby time = 1000 ms
// Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,

unsigned int baroCycleTime = 250;  //Baro cycle time in ms
float baroCompensator = 1485; //Pascals, added to baro value

#endif
//////////////////////////////////////////////////////////////////




float maxCurrentH = 100.0f;  //Max current in "high" mode, selected by red button
float initialCurrentL = 70.0f;    //Max current in "low" mode, default
float maxCurrentOut = initialCurrentL;  //The actual limit to which controls are scaled, chosen with the red button
float maxBrakeCurrent = 100.0f;
const float dutyFadeStart = 0.2f; //After this speed, current starts getting faded
const float dutyFadeCoeff = 1.5f;
//float maxSpeed = 50.0f;
//float maxPower = 400000.0f;

//Pin definitions
//Serial1 - VESC
//Serial2 - Nextion display

const byte brakePin = A11;
const byte throttlePin = A10;
const byte blinkPin = A14;
const byte turboButton = 16;
const byte lightButton = 15;
const byte blinkR_w = 22;
const byte blinkR_y = 21;
const byte blinkL_w = 23;
const byte blinkL_y = 5;
//const byte buck12 = 3;
// CANBUS TX: 3 RX: 4
const byte bucks = 11;
const byte loBeam = 8;
const byte hiBeam = 7;
const byte batt1heat = 20;
const byte batt2heat = 17;
const byte handHeat = 14;
const byte extraHiSide = 13;
const byte piezoPin = 4;
const byte tempSens = 12;
const byte ledpPin = 2;


//Used for calculating velocity
const float wheeldiameter = 0.315;
const float reductionration = (40 * 64) / (15 * 11);
const int motorpoles = 14;
const int encoderPerRot = 42;
const double odoToMeters = ((1.0 / (encoderPerRot)) / reductionration) * wheeldiameter * 3.14159;
const double metersToOdo = 1.0 / odoToMeters;

//const float wheeldiameter = 0.47;
//const float reductionration = 1;
//const int motorpoles = 32;
//const int encoderPerRot = motorpoles*3;  //3 hall sensors
//const double odoToMeters = ((1.0 / (encoderPerRot)) / reductionration) * wheeldiameter * 3.14159;
//const double metersToOdo = 1.0 / odoToMeters;


bool leftBlinkOn = false;
bool rightBlinkOn = false;
bool loBeamOn = false;
bool hiBeamOn = false;
bool battAlarmOn = false;
bool coldAlarmOn = false;
bool humAlarmOn = false;
bool brainHeatOn = false;
bool battHeatOn = false;

float cellVoltage = 0.0;
float rpm = 0.0;
float wheelrpm = 0.0;
float velocity = 0.0;
float power = 0.0;
float BMEtemp = 0.0;
float BMEhum = 0.0;
float BMEpres = 0.0;
float BMEalt = 0.0;
float BMEdew = 0.0;
float BMEsealev = 0.0;

int nextionPage = 0;
int needInitializing = 10;

int brakeStart = 272;  //Above this adc value, there is throttle. Below,it is cut off
int brakeEnd = 700; //Top end of ADC range, anything above this saturates to "full"
int throttleStart = 276;
int throttleEnd = 700;

//int throttleStart = 268;  //Calibrated start and endpoints for control potentiometer ADC readings
//int throttleEnd = 805;
//int brakeStart = 272;
//int brakeEnd = 806;

int throttleLimited = 0;
int brakeLimited = 0;
int throttleNormalized = 0; //Normalized to 0-1000
int brakeNormalized = 0;
float throttle = 0;
float brake = 0;
float engineTractionCurrent = 0; //Calculated traction current sourced from engine

unsigned int buckRequestStamp = 0;  //Whenever something wants to use 12v buck, this is set to current time. If it is too far in past, buck is turned off. Else, it is on.
unsigned int buckRestartStamp = 0; //Set to current when bucks operational, difference between this and current time needs to be more than buckRestartDelay to restart
//unsigned int buck5vRequestStamp = 0;  //same for 5v buck
//unsigned int buck5vRestartStamp = 0;

const unsigned int buckTimeout = 900000; //After this many MS of nothing using bucks, they are turn off (both independently) 15 mins here
const unsigned int buckRestartDelay = 120000; //After turning off bucks (buckTimeout elapsed since something used them), must wait until this time to restart them to let NTC cool down. 2 min here
const unsigned int relayWaitTillOn = 100; //How many MS to wait until powering load after relay has been turned on

const unsigned int blinkPeriod = 400;
bool blinkArmed = false;

struct SaveStruct {
  double totalOdo;
  uint32_t drivingHours;
  uint32_t powerOnHours;
};
SaveStruct saveStruct;

int debugint = 0;
uint8_t rebootReason = 0;
uint32_t maxLoops = 0;

//All of these are in meters
double newOdo = 0; //New odometry (in meters) that has not yet been added to the EEPROM odo counter
double addedOdo = 0; //How much odometry was added during this poweron (of controller, not vesc). Initialized to current vesc odo value at initialization just in case it was on before ctrl
double tripCompensator = 0;
bool odoInitialized = false;

unsigned int freq = 0; //Frequency of beeper, zero means off.
class StoreFilter {
  public:
    StoreFilter(float, float);
    float filteredValue();
    void runEMA(float value);
    float value;
  private:
    float _alpha;
    float _filtered;
};

StoreFilter::StoreFilter(float startValue, float alpha) {
  value = startValue;
  _alpha = alpha;
  value = value;
}
float StoreFilter::filteredValue() {
  return _filtered;
}
void StoreFilter::runEMA(float value) {
  _filtered = (_alpha * value) + ((1 - _alpha) * _filtered);
}

StoreFilter vBattFilter(0, 0.1); //Was 0.05

FlexCAN_T4<CAN0, RX_SIZE_16, TX_SIZE_256> can0; //Start the FlexCAN peripheral

Vescan vesc(3,2); //Initialize the Vescan library. First argument is the VESC's CAN ID, second is the host id (not used currently)
Vescgen genControl;
float virtuaGear = 1.0f;


void irq(const CAN_message_t &ref) {           //This function is set to be called on can message receipt by the FlexCAN library
  vesc.parseStatus(VescCANify(ref));          //Parse the message, if it is a VESC status message, its contents will be decoded into the status struct of the "Vescan" class
  canParsed = true;
}


void setup() {

  rebootReason = returnResetType();

#ifdef BAROENABLE
  Wire.begin();
  delay(100);

  while (!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }

  // bme.chipID(); // Deprecated. See chipModel().
  switch (bme.chipModel())
  {
    case BME280::ChipModel_BME280:
      Serial.println("Found BME280 sensor! Success.");
      break;
    case BME280::ChipModel_BMP280:
      Serial.println("Found BMP280 sensor! No Humidity available.");
      break;
    default:
      Serial.println("Found UNKNOWN sensor! Error!");
  }
#endif

 
  Serial.begin(9600); //Debugserial, usb. Baud doesn't matter
  Serial1.begin(115200);  //For VESC
  Serial2.begin(115200);  //For the Nextion display

  UART.setSerialPort(&Serial1);

  pinMode(brakePin, INPUT);
  pinMode(throttlePin, INPUT);
  pinMode(blinkPin, INPUT);
  pinMode(turboButton, INPUT_PULLUP);
  pinMode(lightButton, INPUT_PULLUP);

  pinMode(blinkR_w, OUTPUT);
  pinMode(blinkR_y, OUTPUT);
  pinMode(blinkL_w, OUTPUT);
  pinMode(blinkL_y, OUTPUT);
  pinMode(bucks, OUTPUT);
  pinMode(loBeam, OUTPUT);
  pinMode(hiBeam, OUTPUT);
  pinMode(batt1heat, OUTPUT);
  pinMode(batt2heat, OUTPUT);
  pinMode(handHeat, OUTPUT);
  pinMode(extraHiSide, OUTPUT);
  pinMode(piezoPin, OUTPUT);


  // ====== CAN ========
  can0.begin();  
  can0.setBaudRate(500000);
  can0.setMaxMB(16);
  can0.enableMBInterrupts();
  can0.onReceive(irq);
  can0.setMB(MB8, TX, STD);
  // ===================

  vesc.setERPMToRPMRatio(7); //Motor pole pairs
  genControl.SetIdleMinRPM(3500);
  genControl.SetCutStartRPM(7700);
  genControl.SetCutEndRPM(8000);
  genControl.SetMaxRPM(8500);
  genControl.SetRPMPerAssist(7500);


  //saveStruct.totalOdo = 50000;
  //EEPROM.put(0,saveStruct);
  EEPROM.get(0, saveStruct); //Load permanent data from EEPROM
  tripCompensator = saveStruct.totalOdo;
}

void loop() {
  // put your main code here, to run repeatedly:
  static unsigned int mainStamp = 0;

  BMEHandle();
  //CANgine();
  //CANGet();
  //CanTest();
  if(canParsed){
    CanReact(true);
    canParsed = false;
  }

  if (micros() - mainStamp > 20000) {
    mainStamp = micros();
    GetVescData();
    InputReader();
    InputReaderForEngine();
    CanReact(false);
    SendVescCommands();
    BuckControl();
    DisplayUpdate();
  }
  EEPROMHandler();
  KickDog();
  //TestPrinter(1, 1);
}

void BMEHandle() {
#ifdef BAROENABLE
  static unsigned int bmeStamp = 0;
  static unsigned int calibStamp = 0;
  static float altCalib = 0.0;
  static float altAccu = 0.0;
  static int altCount = 0;
  static bool calibTrigger = true;

  if (millis() - bmeStamp > baroCycleTime) {
    bmeStamp = millis();

    BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
    BME280::PresUnit presUnit(BME280::PresUnit_Pa);

    bme.read(BMEpres, BMEtemp, BMEhum, tempUnit, presUnit)
    BMEpres += baroCompensator;

    EnvironmentCalculations::AltitudeUnit envAltUnit  =  EnvironmentCalculations::AltitudeUnit_Meters;
    EnvironmentCalculations::TempUnit     envTempUnit =  EnvironmentCalculations::TempUnit_Celsius;

    if (millis() - calibStamp < 10000) {
      BMEalt = EnvironmentCalculations::Altitude(BMEpres, envAltUnit);
      altAccu += BMEalt;
      altCount++;
      calibTrigger = true;
    }
    else {
      if (calibTrigger) {
        altCalib = altAccu / altCount;
        calibTrigger = false;
        altAccu = 0.0;
        altCount = 0.0;
      }
      BMEalt = EnvironmentCalculations::Altitude(BMEpres, envAltUnit) - altCalib;
    }


    BMEdew = EnvironmentCalculations::DewPoint(BMEtemp, BMEhum, envTempUnit);
    BMEsealev = EnvironmentCalculations::EquivalentSeaLevelPressure(BMEalt, BMEtemp, BMEpres);


    //    Serial.print("Temp: ");
    //    Serial.print(BMEtemp);
    //    Serial.print("°" + String(tempUnit == BME280::TempUnit_Celsius ? 'C' : 'F'));
    //    Serial.print("\t\tHumidity: ");
    //    Serial.print(BMEhum);
    //    Serial.print("% RH");
    //    Serial.print("\t\tPressure: ");
    //    Serial.print(BMEpres);
    //    Serial.print(" Pa");
    //    Serial.print("\t\tAltitude: ");
    //    Serial.print(BMEalt);
    //    Serial.print(" m");
    //    Serial.print("\t\tDewpoint: ");
    //    Serial.print(BMEdew);
    //    Serial.print(" C");
    //    Serial.print("\t\tEqSeaLevPres: ");
    //    Serial.print(BMEsealev);
    //    Serial.println(" Pa");
  }
#endif
}

//void GetNumTest() {
//  static uint8_t stage = 0;
//  static int32_t numbuf = 0;
//  while (Serial2.available()) {
//    uint8_t c = Serial2.read();
//
//    switch (stage) {
//      case 0:
//        if ((uint8_t)c == 0x66) {
//          stage = 1;
//          numbuf = 0;
//        }
//        break;
//      case 1:
//        {
//          numbuf = (uint32_t)c;
//          stage = 2;
//        }
//        break;
//      case 2:
//        {
//          if ((uint8_t)c == 0xFF) stage = 3;
//          else stage = 0;
//        }
//        break;
//      case 3:
//        {
//          if ((uint8_t)c == 0xFF) stage = 4;
//          else stage = 0;
//        }
//        break;
//      case 4:
//        {
//          if ((uint8_t)c == 0xFF) {
//            nextionPage = numbuf;
//            stage = 0;
//          }
//          else stage = 0;
//        }
//        break;
//    }
//
//  }
//}

void NextionParser() {
  unsigned int loopies = 0;
  static uint8_t stage = 0;
  static uint8_t which = 0;
  static int32_t numbuf = 0;
  while (loopies < 1000 && Serial2.available()) {
    loopies ++;
    uint8_t c = Serial2.read();
    switch (stage) {
      case 0:
        if ((uint8_t)c == 0x66) {
          stage = 1;
          which = 0; //Parsing page number
          numbuf = 0;

        }
        else if ((uint8_t)c == 0x65) {
          stage = 1;
          which = 1; //Parsing buttons number
          numbuf = 0;
        }
        break;
      case 1:
        {
          if (which == 0) {
            numbuf = (uint32_t)c;
            stage = 2;
          }
          else if (which == 1) {
            if ((uint32_t)c == 0x02) {
              stage = 2;
            }
            else stage = 0;
          }
        }
        break;
      case 2:
        {
          if (which == 0) {

            if ((uint8_t)c == 0xFF) stage = 3;
            else stage = 0;
          }
          else if (which == 1) {
            if ((uint8_t)c == 0x03) {
              initialCurrentL = max(initialCurrentL -= 10.0, 1.0);
            }
            else if ((uint8_t)c == 0x04) {
              initialCurrentL = max(initialCurrentL -= 1.0, 1.0);
            }
            else if ((uint8_t)c == 0x06) {
              initialCurrentL = min(initialCurrentL += 1.0, 100.0);
            }
            else if ((uint8_t)c == 0x05) {
              initialCurrentL = min(initialCurrentL += 10.0, 100.0);
            }
            stage = 0;
          }

        }
        break;
      case 3:
        {
          if ((uint8_t)c == 0xFF) stage = 4;
          else stage = 0;
        }
        break;
      case 4:
        {
          if ((uint8_t)c == 0xFF) {
            nextionPage = numbuf;
            stage = 0;
          }
          else stage = 0;
        }
        break;
    }

  }
  if (loopies > maxLoops) {
    maxLoops = loopies;
  }
}


void DisplayUpdate() {
  //static unsigned int dispStamp = 0;
  static int previousNextionPage = nextionPage;

  if (nextionPage != previousNextionPage) {
    needInitializing = 10;
    previousNextionPage = nextionPage;
  }
  else {
    //needInitializing = false;
  }

  //if (millis() - dispStamp > 20) {

  Serial2.print("sendme"); //Ask display to send some values
  Serial2.print("\xFF\xFF\xFF");

  //================Update every time=================
  //dispStamp = millis();
  GetVescData();
  NextionParser();

  switch (nextionPage) {
    case 0:
      {
        //Grokescreen, do nothing
      } break;
    case 1:
      {
        MainpageUpdate(needInitializing);  //Mainpage
      } break;
    case 2:
      {
        ConfUpdate(needInitializing);  //Configuration page
      } break;
    case 3:
      {
        PowerUpdate(needInitializing);  //Power graph page
      } break;
    case 4:
      {
        TempUpdate(needInitializing);  //Temperature graph page
      } break;
    case 5:
      {
        BaroUpdate(needInitializing);  //Barometer page
      } break;
    case 6:
      {
        //lightpage
      } break;
    case 7:
      {
        //Voltpage
      } break;
    case 8:
      {
        BmsUpdate(needInitializing); //BMS page
      } break;

    default:
      break;
  }


  //}
}

void MainpageUpdate(int needInitializing) { //Updates stuff that is on mainpage
  static unsigned int slowDispStamp = 0;
  static int slowDispCycle = 0;
  char paskuri[20]; //Buffer for float to string conversionss
  int picnum = 0;

  static bool leftBlinkState = true;  //Remember previous state of icon, only send command to redraw on change
  static bool rightBlinkState = true;
  static bool hiBeamState = true;
  static bool loBeamState = true;
  static bool battAlarmState = true;
  static bool coldAlarmState = true;
  static bool humAlarmState = true;
  static bool brainHeatState = true;
  static bool battHeatState = true;

  if (needInitializing) { //Make these true on initializing (display initializes with icons on, so either keep them on or detect change and reset)
    //leftBlinkState = rightBlinkState = hiBeamState = loBeamState = battAlarmState = coldAlarmState = humAlarmState = brainHeatState = battHeatState = true;
  }


  //Speedo
  rpm = ((float)UART.data.rpm) / (motorpoles / 2); //Calculate motor RPM from ERPM with pole pairs
  wheelrpm = rpm / reductionration;               //Calculate wheel RPM with gear reduction ration
  velocity = (wheelrpm * wheeldiameter * 3.14159 / 60) * 3.6; //Calculate velocity with wheel diameter

  int velToGauge = abs((int)(velocity * 100)); //Increase resolution since there are multiple tics per kmh
  picnum = constrain(map(velToGauge, 0, 4000, 10, 110), 10, 111); //speedo images are from 10 to 111 with 111 being the "40+" picture
  Serial2.print("spd.pic="); Serial2.print(picnum); Serial2.print("\xFF\xFF\xFF");


  //Motor current
  picnum = map((int)UART.data.avgMotorCurrent, -80, 120, 112, 212);  //Motor current gauge images go from 112 to 212, 213 and 214 are the "out of range" images
  if (picnum > 212) picnum = 213;
  if (picnum < 112) picnum = 214;
  Serial2.print("motamp.pic="); Serial2.print(picnum); Serial2.print("\xFF\xFF\xFF");

  //Battery current
  picnum = map((int)UART.data.avgInputCurrent, -40, 60, 215, 315);  //Range is from 215 to 315
  if (picnum > 315) picnum = 316;
  if (picnum < 215) picnum = 317;
  Serial2.print("battamp.pic="); Serial2.print(picnum); Serial2.print("\xFF\xFF\xFF");

  //Motor temperatureUART.data.temperatureMotor //The image for 90C is missing, making the range discontinuus :(
  //float bbw = -21.0;
  float moTemPrint = (UART.data.temperatureMotor < 91.0) ? UART.data.temperatureMotor + 0.5 : UART.data.temperatureMotor;
  //float moTemPrint = (bbw < 91.0) ? bbw + 0.5 : bbw;
  picnum = map((int)(moTemPrint * 10), -200, 1000, 318, 437); //Range is from 318 to 437
  if (picnum > 437) picnum = 438;
  if (picnum < 318) picnum = 439;
  Serial2.print("motemp.pic="); Serial2.print(picnum); Serial2.print("\xFF\xFF\xFF");


  //Voltage
  //dtostrf(vBattFilter.filteredValue() / 12, 2, 2, paskuri);
  //Serial2.print("BattCell.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");

  picnum = map((int)(cellVoltage * 1000), 3200, 4200, 439, 541); //Range is 440 - 541 (542 + 543 -)
  if (picnum > 541) picnum = 542;
  if (picnum < 440) picnum = 543;
  Serial2.print("cellvolt.pic="); Serial2.print(picnum); Serial2.print("\xFF\xFF\xFF");


  //Watts
  Serial2.print("Watts.val="); Serial2.print((int)(power)); Serial2.print("\xFF\xFF\xFF");

  //Input visualization
  Serial2.print("Throttle.val="); Serial2.print(map(throttleNormalized, 0, 1000, 100, 0)); Serial2.print("\xFF\xFF\xFF");
  Serial2.print("Brake.val="); Serial2.print(map(brakeNormalized, 0, 1000, 0, 100)); Serial2.print("\xFF\xFF\xFF");



  //Indicators, updated when they change

  if (needInitializing || leftBlinkState != leftBlinkOn) {
    Serial2.print("LeftBlink.picc=");
    Serial2.print(leftBlinkOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    leftBlinkState = leftBlinkOn;
  }
  if (needInitializing || rightBlinkState != rightBlinkOn) {
    Serial2.print("RightBlink.picc=");
    Serial2.print(rightBlinkOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    rightBlinkState = rightBlinkOn;
  }
  if (needInitializing || hiBeamState != hiBeamOn) {
    Serial2.print("HiBeam.picc=");
    Serial2.print(hiBeamOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    hiBeamState = hiBeamOn;
  }
  if (needInitializing || loBeamState != loBeamOn) {
    Serial2.print("LoBeam.picc=");
    Serial2.print(loBeamOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    loBeamState = loBeamOn;
  }
  if (needInitializing || battAlarmState != battAlarmOn) {
    Serial2.print("BattAlarm.picc=");
    Serial2.print(battAlarmOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    battAlarmState = battAlarmOn;
  }
  if (needInitializing || coldAlarmState != coldAlarmOn) {
    Serial2.print("ColdAlarm.picc=");
    Serial2.print(coldAlarmOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    coldAlarmState = coldAlarmOn;
  }
  if (needInitializing || humAlarmState != humAlarmOn) {
    Serial2.print("HumAlarm.picc=");
    Serial2.print(humAlarmOn ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    humAlarmState = humAlarmOn;
  }
  if (needInitializing || brainHeatState != bmsHot) {
    Serial2.print("BrainHeat.picc=");
    Serial2.print(bmsHot ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    brainHeatState = bmsHot;
  }
  if (needInitializing || battHeatState != balancing) {
    Serial2.print("BattHeat.picc=");
    Serial2.print(balancing ? 3 : 4);
    Serial2.print("\xFF\xFF\xFF");
    battHeatState = balancing;
  }

  if (needInitializing) {
    needInitializing--;
  }


  if (millis() - slowDispStamp > 250) {
    slowDispStamp = millis();
    int odoPrinter = 0;
    switch (slowDispCycle) {
      //Erpm, tachometer, mah in, mah out
      case 0:
        slowDispCycle = 1;
        //Serial2.print("erpm.val="); Serial2.print(UART.data.rpm); Serial2.print("\xFF\xFF\xFF");
        //Serial2.print("tach.val="); Serial2.print((int)UART.data.tachometerAbs); Serial2.print("\xFF\xFF\xFF");
        Serial2.print("n0.val="); Serial2.print((int)(UART.data.wattHours * 1.0)); Serial2.print("\xFF\xFF\xFF");
        Serial2.print("n1.val="); Serial2.print((int)(UART.data.wattHoursCharged * 1.0)); Serial2.print("\xFF\xFF\xFF");
        break;

      case 1:
        slowDispCycle = 2;
        //Odometry
        odoPrinter = (((int32_t)saveStruct.totalOdo) + newOdo) / 10;
        Serial2.print("Odo.val="); Serial2.print(odoPrinter); Serial2.print("\xFF\xFF\xFF");

        odoPrinter = (((int32_t)saveStruct.totalOdo) + newOdo - tripCompensator) / 10;
        Serial2.print("TripOdo.val="); Serial2.print(odoPrinter); Serial2.print("\xFF\xFF\xFF");

        dtostrf(bmsChipTempHi, 6, 2, paskuri);
        //Serial2.print("BrainTemp.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
        Serial2.print("BrainTemp.txt=\""); Serial2.print(vesc.status.amp_hours_charged); Serial2.print("\"\xFF\xFF\xFF");

        dtostrf(BMEtemp, 6, 2, paskuri);
        //Serial2.print("BattTempH.txt=\""); Serial2.print(highCell); Serial2.print("\"\xFF\xFF\xFF");
        Serial2.print("BattTempH.txt=\""); Serial2.print(vesc.status.temp_motor); Serial2.print("\"\xFF\xFF\xFF");

        dtostrf(BMEtemp, 6, 2, paskuri);
        //Serial2.print("BattTempL.txt=\""); Serial2.print(lowCell); Serial2.print("\"\xFF\xFF\xFF");
        Serial2.print("BattTempL.txt=\""); Serial2.print(vesc.status.rpm); Serial2.print("\"\xFF\xFF\xFF");

        dtostrf(bmsTemp, 6, 2, paskuri);
        //Serial2.print("BrainHum.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
        Serial2.print("BrainHum.txt=\""); Serial2.print(UART.data.ampHours); Serial2.print("\"\xFF\xFF\xFF");

        //Serial2.print("Altimeter.val="); Serial2.print((int)(rebootReason)); Serial2.print("\xFF\xFF\xFF");
        //Serial2.print("Altimeter.val="); Serial2.print((int)(vesc.status.rpm*100)); Serial2.print("\xFF\xFF\xFF");
        Serial2.print("Altimeter.val="); Serial2.print( (int) (-vesc.status.power*100) ); Serial2.print("\xFF\xFF\xFF");
        //Serial2.print("Altimeter.val="); Serial2.print(100); Serial2.print("\xFF\xFF\xFF");
        break;

      case 2:

        slowDispCycle = 0;
        break;

      default:
        slowDispCycle = 0;
        break;
    }
  }
}

void ConfUpdate(int needInitializing) {
  Serial2.print("curlim.val="); Serial2.print((int32_t)initialCurrentL); Serial2.print("\xFF\xFF\xFF");
}

void PowerUpdate(int needInitializing) {
  static unsigned int powerStamp = 0;
  if (millis() - powerStamp > 100) {
    powerStamp = millis();

    //Motor and battery current graphs
    Serial2.print("add 2,0,"); Serial2.print(constrain(map((int)(UART.data.avgMotorCurrent), -60, 100, 0, 160), 0, 160)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 2,1,"); Serial2.print(constrain(map((int)(UART.data.avgInputCurrent), -60, 100, 0, 160), 0, 160)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 2,2,"); Serial2.print(constrain(map((int)(-vesc.status.battery_current_avg), -60, 100, 0, 160), 0, 160)); Serial2.print("\xFF\xFF\xFF");


    //Power graph
    if (power > 0) { //Power out
      Serial2.print("add 3,0,"); Serial2.print(constrain(map((int)(power), 0, 1000, 0, 80), 0, 80)); Serial2.print("\xFF\xFF\xFF");
      Serial2.print("add 3,1,"); Serial2.print(0); Serial2.print("\xFF\xFF\xFF");
    }
    else {  //Power in
      Serial2.print("add 3,1,"); Serial2.print(constrain(map((int)(-power), 0, 1000, 0, 80), 0, 80)); Serial2.print("\xFF\xFF\xFF");
      Serial2.print("add 3,0,"); Serial2.print(0); Serial2.print("\xFF\xFF\xFF");
    }

    if(vesc.status.power > 0){
      Serial2.print("add 3,2,"); Serial2.print(constrain(map((int)(vesc.status.power), 0, 1000, 0, 80), 0, 80)); Serial2.print("\xFF\xFF\xFF");
      Serial2.print("add 3,3,"); Serial2.print(0); Serial2.print("\xFF\xFF\xFF");
    }
    else{
      Serial2.print("add 3,3,"); Serial2.print(constrain(map((int)(-vesc.status.power), 0, 1000, 0, 80), 0, 80)); Serial2.print("\xFF\xFF\xFF");
      Serial2.print("add 3,2,"); Serial2.print(0); Serial2.print("\xFF\xFF\xFF");
    }

    //Power readout
    Serial2.print("WattDisplay.val="); Serial2.print((int)(power)); Serial2.print("\xFF\xFF\xFF");
    //Current readout
    Serial2.print("AmpDisplay.val="); Serial2.print((int)(UART.data.avgInputCurrent / 10.0)); Serial2.print("\xFF\xFF\xFF");
    //Duty readout
    Serial2.print("DutyDisplay.val="); Serial2.print((int)(UART.data.dutyCycleNow * 10)); Serial2.print("\xFF\xFF\xFF");

  }
}

void TempUpdate(int needInitializing) {
  char paskuri[20]; //Buffer for float to string conversionss
  static unsigned int tempStamp = 0;
  if (millis() - tempStamp > 1000) {
    tempStamp = millis();
    //Temp graph 1: board, motor, VESC
    Serial2.print("add 17,0,"); Serial2.print(constrain(map((int)(vesc.status.temp_motor * 10), -200, 1000, 0, 120), 0, 120)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 17,1,"); Serial2.print(constrain(map((int)(UART.data.temperatureMotor * 10), -200, 1000, 0, 120), 0, 120)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 17,2,"); Serial2.print(constrain(map((int)(UART.data.temperatureFet * 10), -200, 1000, 0, 120), 0, 120)); Serial2.print("\xFF\xFF\xFF");

    //Temp graph 2: Battery 1 top avg, battery 1 bottom average, battery 2 top average, battery 2 bottom average
    Serial2.print("add 18,0,"); Serial2.print(vesc.status.temp_fet); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 18,1,"); Serial2.print(25); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 18,2,"); Serial2.print(70); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 18,3,"); Serial2.print(90); Serial2.print("\xFF\xFF\xFF");

    //Temp graph 1 numbers
    dtostrf(BMEtemp, 6, 2, paskuri);  Serial2.print("tBrain.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
    dtostrf(UART.data.temperatureMotor, 6, 2, paskuri);  Serial2.print("tMot.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
    dtostrf(UART.data.temperatureFet, 6, 2, paskuri);  Serial2.print("tVesc.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");

    //Temp graph 2 numbers
    //dtostrf(BMEtemp, 7, 2, paskuri);  Serial2.print("tBrain.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");


  }
}
void BaroUpdate(int needInitializing) {
#ifdef BAROENABLE
  static unsigned int baroStamp = 0;
  static unsigned int slowBaroStamp = 0;
  if (millis() - baroStamp > baroCycleTime) {
    baroStamp = millis();



    Serial2.print("DevPoint.val="); Serial2.print((int)(BMEdew * 100)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("Pascals.val="); Serial2.print((int)BMEpres); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("BrainTemp.val="); Serial2.print((int)(BMEtemp * 100)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("PascalsEq.val="); Serial2.print((int)BMEsealev); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("Alt.val="); Serial2.print((int)(BMEalt * 100)); Serial2.print("\xFF\xFF\xFF");
  }
  if (millis() - slowBaroStamp > 2000) {
    slowBaroStamp = millis();
    Serial2.print("add 7,0,"); Serial2.print(constrain(map((int)(BMEalt), -60, 100, 0, 160), 0, 160)); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("add 7,1,"); Serial2.print(constrain(map((int)(BMEtemp), -60, 100, 0, 160), 0, 160)); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("add 9,0,"); Serial2.print(constrain(map((int)(BMEhum), 0, 100, 0, 80), 0, 80)); Serial2.print("\xFF\xFF\xFF");
  }
#endif
}

void BmsUpdate(int needInitializing){

  //Cell voltages
  char paskuri[20];
  if(bmsGenericUpdated){
    bmsGenericUpdated = false;
    
    dtostrf(fabs(packPower), 3, 0, paskuri);
    Serial2.print("cpow.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
    Serial2.print("t1.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");

    dtostrf(fabs(packCurrent), 3, 1, paskuri);
    Serial2.print("ccur.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
    dtostrf(packCurrent, 6, 2, paskuri);
    Serial2.print("t2.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");

    //Colors for currents
    Serial2.print("ccur.pco="); Serial2.print(packPower > 0 ? green : red); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cpow.pco="); Serial2.print(packCurrent > 0 ? green : red); Serial2.print("\xFF\xFF\xFF");

    dtostrf(packVolts, 6, 2, paskuri);
    Serial2.print("t0.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");

    //Power graph
    Serial2.print("add 19,0,"); Serial2.print((int)constrain(map(packPower,0,450, 0, 85),0,85)); Serial2.print("\xFF\xFF\xFF"); //Total power in and out of battery pack
    Serial2.print("add 19,1,"); Serial2.print((int)constrain(map(packPower,0,-450, 0, 85),0,85)); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("add 19,2,"); Serial2.print((int)constrain(map(externalPower,0,450, 0, 85),0,85)); Serial2.print("\xFF\xFF\xFF"); //Power to/from battery excluding VESC, ie. solar charger or ext. load
    Serial2.print("add 19,3,"); Serial2.print((int)constrain(map(externalPower,0,-450, 0, 85),0,85)); Serial2.print("\xFF\xFF\xFF");

    // Balancing or not
    Serial2.print("cell0.pco="); Serial2.print(cellBalancing[0] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c0v.pco="); Serial2.print(cellBalancing[0] ? red : blue); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell1.pco="); Serial2.print(cellBalancing[1] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c1v.pco="); Serial2.print(cellBalancing[1] ? red : turq); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell2.pco="); Serial2.print(cellBalancing[2] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c2v.pco="); Serial2.print(cellBalancing[2] ? red : blue); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell3.pco="); Serial2.print(cellBalancing[4] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c3v.pco="); Serial2.print(cellBalancing[4] ? red : turq); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell4.pco="); Serial2.print(cellBalancing[5] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c4v.pco="); Serial2.print(cellBalancing[5] ? red : blue); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell5.pco="); Serial2.print(cellBalancing[6] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c5v.pco="); Serial2.print(cellBalancing[6] ? red : turq); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell6.pco="); Serial2.print(cellBalancing[7] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c6v.pco="); Serial2.print(cellBalancing[7] ? red : blue); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell7.pco="); Serial2.print(cellBalancing[9] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c7v.pco="); Serial2.print(cellBalancing[9] ? red : turq); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell8.pco="); Serial2.print(cellBalancing[10] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c8v.pco="); Serial2.print(cellBalancing[10] ? red : blue); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell9.pco="); Serial2.print(cellBalancing[11] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c9v.pco="); Serial2.print(cellBalancing[11] ? red : turq); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell10.pco="); Serial2.print(cellBalancing[12] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c10v.pco="); Serial2.print(cellBalancing[12] ? red : blue); Serial2.print("\xFF\xFF\xFF");

    Serial2.print("cell11.pco="); Serial2.print(cellBalancing[14] ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c11v.pco="); Serial2.print(cellBalancing[14] ? red : turq); Serial2.print("\xFF\xFF\xFF");

    //Inhibit buttons
    Serial2.print("dsgen.bco="); Serial2.print(dischargeInhibited ? red : green); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("chgen.bco="); Serial2.print(chargeInhibited ? red : green); Serial2.print("\xFF\xFF\xFF");

  }
  if(bmsCells1_5Updated){
    bmsCells1_5Updated = false;
    Serial2.print("c0v.val="); Serial2.print((int)cellVolts[0]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c1v.val="); Serial2.print((int)cellVolts[1]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c2v.val="); Serial2.print((int)cellVolts[2]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c3v.val="); Serial2.print((int)cellVolts[4]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell0.val="); Serial2.print( constrain( map(cellVolts[0], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell1.val="); Serial2.print( constrain( map(cellVolts[1], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell2.val="); Serial2.print( constrain( map(cellVolts[2], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell3.val="); Serial2.print( constrain( map(cellVolts[4], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    
  }
  if(bmsCells6_10Updated){
    bmsCells6_10Updated = false;
    Serial2.print("c4v.val="); Serial2.print((int)cellVolts[5]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c5v.val="); Serial2.print((int)cellVolts[6]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c6v.val="); Serial2.print((int)cellVolts[7]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c7v.val="); Serial2.print((int)cellVolts[9]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell4.val="); Serial2.print( constrain( map(cellVolts[5], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell5.val="); Serial2.print( constrain( map(cellVolts[6], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell6.val="); Serial2.print( constrain( map(cellVolts[7], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell7.val="); Serial2.print( constrain( map(cellVolts[9], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
  }
  if(bmsCells11_15Updated){
    bmsCells11_15Updated = false;
    Serial2.print("c8v.val="); Serial2.print((int)cellVolts[10]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c9v.val="); Serial2.print((int)cellVolts[11]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c10v.val="); Serial2.print((int)cellVolts[12]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("c11v.val="); Serial2.print((int)cellVolts[14]); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell8.val="); Serial2.print( constrain( map(cellVolts[10], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell9.val="); Serial2.print( constrain( map(cellVolts[11], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell10.val="); Serial2.print( constrain( map(cellVolts[12], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
    Serial2.print("cell11.val="); Serial2.print( constrain( map(cellVolts[14], 3200, 4200, 0, 100) ,0,100) ); Serial2.print("\xFF\xFF\xFF");
  }
  if(bmsEnergyUpdated){
    bmsEnergyUpdated = false;
    float printEnergy = ( (float)packEnergy )/1000000.0f;
    dtostrf(printEnergy, 7, 3, paskuri);
    Serial2.print("whcur.txt=\""); Serial2.print(paskuri); Serial2.print("\"\xFF\xFF\xFF");
    
  }
  
}

void GetVescData() {
  //static unsigned int vescStamp = 0;
  //if(millis()-vescStamp > 10){
  //  vescStamp = millis();
  if ( UART.getVescValues() ) {
    vBattFilter.runEMA(UART.data.inpVoltage);
    power = UART.data.avgInputCurrent * vBattFilter.filteredValue();
    cellVoltage = vBattFilter.filteredValue() / 12.0; //12 cell battery

    if (!odoInitialized) { //initialize odometry in case VESC already has odo counts when starting
      addedOdo = odoToMeters * (abs(UART.data.tachometerAbs));
      tripCompensator = saveStruct.totalOdo + newOdo;
      odoInitialized = true;
    }
    else { //If odometry is initialized
      newOdo = abs(odoToMeters * UART.data.tachometerAbs - addedOdo); //Difference from last time we saved this;
      //      if (digitalRead(lButton)) {
      //        if (Serial) {
      //          Serial.println("Reset tripmeter!");
      //        }
      //        tripCompensator = saveStruct.totalOdo + newOdo;
      //      }
    }

    if (cellVoltage < 3.6f) battAlarmOn = true;
    else battAlarmOn = false;
  }
  else {
    if (Serial) {
      //Serial.println("Failed to get data!");
    }
  }
  //}
}

void SendVescCommands() {
  //static unsigned int sendStamp = 0;
  // if (micros() - sendStamp >= 1700) {
  //sendStamp = micros();
  //InputReader();
  //    static float velScale = 1.0; //Starting velocity scaling coefficient (for speed limiting)
  //    static float powerScale = 1.0;
  //
  //    float alpha = 0.07;  //Aggressiveness of low pass filter, smaller is faster
  //    float velScaleNew = 1.0 - min(max(velocity - (maxSpeed - 1), 0.0) * 0.7, 1.0); //Calculate new speed limiting term, constrained to 0-1
  //    velScale =  velScale * (1.0 - alpha) + velScaleNew * alpha; //Run the filtering to take care of motor dynamics
  //
  //    float alpha2 = 0.02;  //Same as above but for power
  //    float powerScaleNew = 1.0 - min(max(max(power, 0.0) - (maxPower - 10), 0.0) * 0.05, 1.0);
  //    powerScale = powerScale * (1.0 - alpha2) + powerScaleNew * alpha2;

  if (brakeNormalized > 10) {
    UART.setBrakeCurrent(brake);
  }
  else {
    if(genControl.GetAutoMode() || vesc.status.rpm < 700){
       UART.setCurrent(throttle); 
    }
    else{
      UART.setCurrent(engineTractionCurrent); //Engine testing
    }
   
    
  }
  //}
}


void InputReader() {
  static unsigned int loBeamWantStamp = millis();
  static unsigned int hiBeamWantStamp = millis();
  //static unsigned int parkLightWantStamp = millis();
  static unsigned int blinkLWantStamp = millis();
  static unsigned int blinkRWantStamp = millis();
  static unsigned int visLightWantStamp = millis();
  static unsigned int blinkStamp = 0; //Used to .. time blinks
  static unsigned int debounceStamp = 0;
  static unsigned int btnTimeStamp = 0;

  static bool blinkNow = false;
  static bool blinkLTriggered = false;
  static bool blinkRTriggered = false;
  static int lightState = 0;
  static bool btnPressed = false;
  bool wingLightsOn = false; //If true, turns on white wing lights on handlebar

  /*
    0 = no headlights - wing lights also off
    1 = low beam - low beam and wing lights on
    2 = hi beam - high beam and wing lights on
    3 = visibility lighting - parking light + wing lights on
  */


  throttleLimited = constrain(analogRead(throttlePin), throttleStart, throttleEnd); //Make sure these are in expected ranges
  brakeLimited = constrain(analogRead(brakePin), brakeStart, brakeEnd);

  bool shortPressed = false;
  bool longPressed = false;

  if (!btnPressed) {          //When button is in the "up" debounced state
    if (digitalRead(lightButton)) { //If button up, reset transition timer
      debounceStamp = millis();
    }
    if (millis() - debounceStamp > 20) {  //Move to "down" state and reset debounce timer
      btnPressed = true;
      debounceStamp = millis();
      btnTimeStamp = millis();
    }
  }
  else {        //When button is in the "down" debounce state
    if (!digitalRead(lightButton)) { //If button down, reset transition timer
      debounceStamp = millis();
    }
    if (millis() - debounceStamp > 20) {  //Move to "up" state and reset timer, and generate button length result
      btnPressed = false;
      debounceStamp = millis();
      if (millis() - btnTimeStamp > 270) {
        //longPressed = true;
      }
      else {
        //shortPressed = true;
      }

    }
  }


  switch (lightState) {

    case 0:  //0 = no headlights - wing lights also off
      loBeamWantStamp = millis();
      digitalWrite(loBeam, LOW);
      loBeamOn = false;
      hiBeamWantStamp = millis(); //Update this
      digitalWrite(hiBeam, LOW); //Turn off light
      hiBeamOn = false;
      visLightWantStamp = millis();
      digitalWrite(extraHiSide, LOW);

      wingLightsOn = false;

      if (shortPressed) lightState = 1;
      if (longPressed) lightState = 3;
      break;

    case 1:  //1 = low beam - low beam and wing lights on
      buckRequestStamp = millis(); //Turn buck on

      if (millis() - loBeamWantStamp > relayWaitTillOn) {
        digitalWrite(loBeam, HIGH);
        loBeamOn = true;
        wingLightsOn = true;
      }
      hiBeamWantStamp = millis(); //Update this
      digitalWrite(hiBeam, LOW); //Turn off light
      hiBeamOn = false;
      visLightWantStamp = millis();
      digitalWrite(extraHiSide, LOW);



      if (shortPressed) lightState = 2;
      if (longPressed) lightState = 0;
      break;

    case 2:   //2 = hi beam - high beam and wing lights on
      buckRequestStamp = millis(); //Turn buck on

      loBeamWantStamp = millis();
      digitalWrite(loBeam, LOW);
      loBeamOn = false;

      if (millis() - hiBeamWantStamp > relayWaitTillOn) {
        digitalWrite(hiBeam, HIGH);
        hiBeamOn = true;
        wingLightsOn = true;
      }
      visLightWantStamp = millis();
      digitalWrite(extraHiSide, LOW);


      if (shortPressed) lightState = 1;
      if (longPressed) lightState = 0;
      break;

    case 3:   //3 = visibility lighting - parking light + wing lights on
      buckRequestStamp = millis(); //Turn buck on
      loBeamWantStamp = millis();
      digitalWrite(loBeam, LOW);
      loBeamOn = false;
      hiBeamWantStamp = millis(); //Update this
      digitalWrite(hiBeam, LOW); //Turn off light
      hiBeamOn = false;

      if (millis() - visLightWantStamp > relayWaitTillOn) {
        digitalWrite(extraHiSide, HIGH);
        wingLightsOn = true;
      }


      if (shortPressed) lightState = 1;
      if (longPressed) lightState = 0;
      break;
    default:
      // should never go here
      break;
  }

  //  -- Blinker left ---
  int blinkInput = analogRead(blinkPin);
  blinkInput = 0;

  if ( blinkInput > 800) {
    blinkArmed = true;
  }

  if (blinkArmed && blinkInput > 500 && blinkInput < 700) {  //Blink switch on!

    buckRequestStamp = millis();
    if (millis() - blinkLWantStamp > relayWaitTillOn) {
      if (!blinkLTriggered) {
        blinkLTriggered = true;
        blinkNow = true;
        blinkStamp = millis();
      }
      digitalWrite(blinkL_y, blinkNow);
      leftBlinkOn = blinkNow;
      if (millis() - blinkStamp > blinkPeriod) {
        blinkStamp = millis();
        blinkNow = !blinkNow;
      }

    }
  }
  else {
    leftBlinkOn = false;
    blinkLWantStamp = millis();  //Update this
    digitalWrite(blinkL_y, LOW);
    blinkLTriggered = false;
  }

  //  -- Blinker right ---
  if (blinkArmed && blinkInput > 200 && blinkInput < 500) {   //Blink switch on!

    buckRequestStamp = millis();
    if (millis() - blinkRWantStamp > relayWaitTillOn) {
      if (!blinkRTriggered) {
        blinkRTriggered = true;
        blinkNow = true;
        blinkStamp = millis();
      }
      digitalWrite(blinkR_y, blinkNow);
      rightBlinkOn = blinkNow;
      if (millis() - blinkStamp > blinkPeriod) {
        blinkStamp = millis();
        blinkNow = !blinkNow;
      }
    }
  }
  else {
    rightBlinkOn = false;
    blinkRWantStamp = millis();  //Update this
    digitalWrite(blinkR_y, LOW);
    blinkRTriggered = false;
  }

  if (wingLightsOn) {
    digitalWrite(blinkL_w, !leftBlinkOn);
    digitalWrite(blinkR_w, !rightBlinkOn);
  }
  else {
    digitalWrite(blinkL_w, LOW);
    digitalWrite(blinkR_w, LOW);
  }

  //The motor current limit gets linrearly lower as duty cycle increases, basically a power limit since Ibatt = Imot*duty
  if (digitalRead(turboButton)) {
    //maxCurrentOut = initialCurrentL;  //Flat limit
    //maxCurrentOut = constrain(initialCurrentL*(1.0f-UART.data.dutyCycleNow) , 0.0f, initialCurrentL);  //Original scale
    float scaledNumber = 0.0f;
    if (UART.data.dutyCycleNow < dutyFadeStart) {
      scaledNumber = 0.0f;
    }
    else {
      scaledNumber = UART.data.dutyCycleNow - dutyFadeStart;
    }
    maxCurrentOut = constrain(initialCurrentL * (1.0f - (scaledNumber * dutyFadeCoeff)) , 0.0f, initialCurrentL);
  }
  else {//If it is, current limit is the "high" one
    maxCurrentOut = maxCurrentH;
  }


  throttleNormalized = map(throttleLimited, throttleStart, throttleEnd, 0, 1000);
  brakeNormalized = map(brakeLimited, brakeStart, brakeEnd, 0, 1000);
  throttle = mapf((float)throttleNormalized, 0, 1000, 0, maxCurrentOut);
  //throttle = 0.0f;  //Disable traction motor for testing
  brake = mapf((float)brakeNormalized, 0, 1000, 0, maxBrakeCurrent);
}

float Gearbox(bool turboBtn, int blinkSwitch){
  float generator = -999.0f;
  float cvt = -600.0f;
  //float neutral = 0.0f;
  float gear1 = 2.2f;
  float gear2 = 1.95f;
  float gear3 = 1.4f;
  float gear4 = 1.0f;

//blinkInput > 500 && blinkInput < 700)
  if(blinkSwitch > 500 && blinkSwitch < 700){ //Blinker switch left
    if(turboBtn){
      return gear2;
    }
    else {
      return gear1;
    }
  }
  else if (blinkSwitch > 200 && blinkSwitch < 500){ //Blinker switch right
    if(turboBtn){
      return gear3;
    }
    else {
      return gear4;
    }
  }
  else{
     if(turboBtn){
      return generator;
    }
    else {
      return cvt;
    }
  }


  return 1.0f;
}

void InputReaderForEngine(){
  static bool wantStart = true;
  static unsigned long releaseTime = 0;
  float currentCommand = 0.0f;
  bool buttonPressed = !analogRead(lightButton);

  if(wantStart){
    if(buttonPressed){
      releaseTime = millis();
      currentCommand = 35.0f;  //Crank engine
      genControl.RequestStartup();
    }
  }
  else{
    if(buttonPressed){
      releaseTime = millis();
      currentCommand = -15.0f;  //Slow down engine
      genControl.RequestShutdown();
    }
  }
  if(!buttonPressed){
    genControl.NoShutdownOrStartup();
  }
  virtuaGear = Gearbox(!digitalRead(turboButton), analogRead(blinkPin));
  genControl.SetOutputRatio(virtuaGear);


  if(millis() - releaseTime > 500){  //If some since button released, figure out what is next
    wantStart = vesc.status.rpm < 700;
  }

  CAN_message_t power_message = FlexCANify(vesc.setMaxPower(genControl.VescMaxPower()));
  can0.write(power_message);

  if(genControl.ActivateLoad()){
      CAN_message_t pid_message = FlexCANify(vesc.setRPM(genControl.ApplyLoad()));
      can0.write(pid_message);
  }
  else{
      CAN_message_t out_msg = FlexCANify(vesc.setCurrent(currentCommand)); //Create a duty cycle command message, and convert it to the implementation's format.
      can0.write(out_msg);                                      //Send the message over canbus
  }


  float servoRequest;
  if(brakeNormalized > 10){
    servoRequest = 0.0f;
  }
  else{
    servoRequest = (float)throttleNormalized / 1000.0f;
  }

  genControl.RequestSourceEffort(servoRequest);
  
}

void CanReact(bool immediate){
  static unsigned long lastCalled = 0;
  if(!immediate && micros() - lastCalled < 19000){
    return;
  }
  lastCalled = micros();
  genControl.UpdateRPM(vesc.status.rpm);
  genControl.UpdateOutputRPM((float)UART.data.rpm/7.0f);

  
  float throttleServoPos = genControl.GetModifiedSourceEffort();
  CAN_message_t servo_msg = FlexCANify(vesc.setServo(throttleServoPos));
  can0.write(servo_msg);

  //Ibatt = Imot*duty    igen*dgen = itrac*dtrac -> (igen * dgen) / dtrac
  engineTractionCurrent = -(vesc.status.motor_current_avg * vesc.status.duty_cycle) /  max(UART.data.dutyCycleNow, 0.05);
  engineTractionCurrent *= 0.6f;

  if(1 || velocity < 10.0f){ //At low vel, do not "engine brake"
    engineTractionCurrent = constrain(engineTractionCurrent, 0.0f, initialCurrentL);
  }
  else{  //Else allow some "engine braking"
    engineTractionCurrent = constrain(engineTractionCurrent, -10.0f, initialCurrentL);
  }
  
}

//void BuckRateLimit5(bool on) {
//  static bool onNow = false;
//  static bool ntcCool = true;
//  static unsigned int stamp = 0;
//
//  if (!ntcCool) {
//    if (millis() - buck5vRestartStamp > buckRestartDelay) {
//      ntcCool = true;
//    }
//  }
//
//
//  if (onNow) { //If currently on..
//    if (on) { //And wants to be kept on, do nothing
//      digitalWrite(buck5, HIGH);
//    }
//    else { //if wants to be turned off..
//      if (millis() - stamp > 100) { //And it's 100 ms from previous transition..
//        buck5vRestartStamp = millis(); // ..do so and remember when
//        stamp = millis();
//        digitalWrite(buck5, LOW);
//        onNow = false;
//      }
//    }
//  }
//  else { //If currently off..
//    if (!on) { //and wants to stay off, do nothing
//      digitalWrite(buck5, LOW);
//    }
//    else { //If wants to be on,
//      if (millis() - stamp > 100) { //Needs to be more than 100 ms from previous transition..
//        if (ntcCool) { //And ntc is considered cool, allow for restart
//          buck5vRestartStamp = millis(); // ..do so and remember when
//          stamp = millis();
//          digitalWrite(buck5, HIGH);
//          onNow = true;
//          ntcCool = false; //NTC is no longer cool!
//        }
//
//      }
//    }
//  }
//}
void BuckRateLimit(bool on) {
  static bool onNow = false;
  static bool ntcCool = true;
  static unsigned int stamp = 0;

  if (!ntcCool) {
    if (millis() - buckRequestStamp > buckRestartDelay) {
      ntcCool = true;
    }
  }


  if (onNow) { //If currently on..
    if (on) { //And wants to be kept on, do nothing
      digitalWrite(bucks, HIGH);
      digitalWrite(handHeat, HIGH); // Keep handheater connection powered whenever buck is too
    }
    else { //if wants to be turned off..
      if (millis() - stamp > 100) { //And it's 100 ms from previous transition..
        buckRequestStamp = millis(); // ..do so and remember when
        stamp = millis();
        digitalWrite(bucks, LOW);
        digitalWrite(handHeat, LOW); // Keep handheater connection off whenever buck is too
        onNow = false;
      }
    }
  }
  else { //If currently off..
    if (!on) { //and wants to stay off, do nothing
      digitalWrite(bucks, LOW);
      digitalWrite(handHeat, LOW); // Keep handheater connection off whenever buck is too
    }
    else { //If wants to be on,
      if (millis() - stamp > 100) { //Needs to be more than 100 ms from previous transition..
        if (ntcCool) { //And ntc is considered cool, allow for restart
          buckRequestStamp = millis(); // ..do so and remember when
          stamp = millis();
          digitalWrite(bucks, HIGH);
          digitalWrite(handHeat, HIGH); // Keep handheater connection powered whenever buck is too
          onNow = true;
          ntcCool = false; //NTC is no longer cool!
        }
      }
    }
  }
}

void BuckControl() {
  if (millis() - buckRequestStamp < buckTimeout && millis() > 2000) {
    BuckRateLimit(HIGH);  //Use these to limit relay state changes in case of faults
  }
  else {
    BuckRateLimit(LOW);

  }

  //  if (millis() - buck5vRequestStamp < buckTimeout && millis() > 2000) {
  //    BuckRateLimit5(HIGH);
  //  }
  //  else {
  //    BuckRateLimit5(LOW);
  //
  //  }

}

void TestPrinter(float brake, float throttle) {
  static unsigned int printStamp = 0;
  if (Serial && millis() - printStamp > 200) {
    printStamp = millis();
    //Serial.print(leftBlinkOn); Serial.print('\t'); Serial.print(rightBlinkOn); Serial.print('\t'); Serial.println(analogRead(blinkPin));
    //Serial.print(throttleNormalized); Serial.print('\t'); Serial.println(brakeNormalized);
    //Serial.print(digitalRead(turboButton)); Serial.print('\t'); Serial.print(digitalRead(lightButton)); Serial.print('\t'); Serial.println(nextionPage);
    //Serial.print(analogRead(throttlePin)); Serial.print('\t'); Serial.println(analogRead(brakePin));
    //Serial.println(millis()-buck12vRequestStamp);
    //Serial.println(digitalRead(bucks));
    //Serial.println(canTestVal);
    //Serial.print(odoToMeters*UART.data.tachometerAbs); Serial.print('\t');Serial.print(saveStruct.totalOdo); Serial.print('\t');

    //Odometry debug
    //Serial.print(saveStruct.totalOdo); Serial.print('\t'); Serial.print(UART.data.tachometerAbs); Serial.print('\t'); Serial.print(newOdo); Serial.print('\t'); Serial.print(addedOdo);
    //Serial.print('\t'); Serial.print(tripCompensator); Serial.print('\t'); Serial.println(odoInitialized);
    //Serial.print(debugint); Serial.print('\t'); Serial.println(cellVoltage);
    //    if(digitalRead(lButton)) freq = 0;
    //    else if(digitalRead(lmButton)) freq = 5000;
    //    else if(digitalRead(rmButton)) freq = 8000;
    //    else if(digitalRead(rButton)) freq = 10000;/
    //Serial.print(brake); Serial.print('\t'); Serial.println(throttle);
  }


}



double mapf(double val, double in_min, double in_max, double out_min, double out_max) {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

//newOdo = new counts since update of EEPROM
//addedOdo = Amount of current absolute VESC counts that have already been added to eeprom

void EEPROMHandler() {
  static unsigned int eepromStamp = 0;
  static unsigned int oldTotalOdo = saveStruct.totalOdo; //Used to only save changed values

  if (millis() - eepromStamp > 60000 && odoInitialized) { //Every minute, save data to the EEPROM. There is wear leveling happening so this should be OK.
    eepromStamp = millis();
    //uint32_t newOdo = (int)(odoToMeters*(UART.data.tachometerAbs - addedOdo));  //Difference from last time we saved this;
    saveStruct.totalOdo += newOdo;  //Add the difference
    addedOdo = odoToMeters * ((double)UART.data.tachometerAbs); // Remember where we are now

    if (abs(saveStruct.totalOdo - oldTotalOdo) > 1.0) { //If odometry has changed from last save, save it
      EEPROM.put(0, saveStruct);
      oldTotalOdo = saveStruct.totalOdo;
    }

  }
}

void CanTest() {
  // static uint32_t canStamp = 0;
  // static uint64_t inc = 0;
  // if (millis() - canStamp > 1000) {

  //   canStamp = millis();

  //   msg.ext = 0;
  //   msg.id = 0x100;
  //   msg.len = 8;

  //   msg.buf[0] = inc;
  //   msg.buf[1] = inc >> 8;
  //   msg.buf[2] = inc >> 16;
  //   msg.buf[3] = inc >> 24;
  //   msg.buf[4] = inc >> 32;
  //   msg.buf[5] = inc >> 40;
  //   msg.buf[6] = inc >> 48;
  //   msg.buf[7] = inc >> 56;

  //   Can0.write(msg);
  //   inc++;
  // }
}

void CANGet() {
  // CAN_message_t inMsg;
  // if (Can0.available())
  // {
  //   Can0.read(inMsg);
  //   //
  //   if (inMsg.id == 0x20) {  // Generic status
  //     bmsGenericUpdated = true;
  //     canTestVal = (int32_t)( ((uint32_t)inMsg.buf[2] << 8) | (uint32_t)inMsg.buf[3] ) / 10;
  //     packVolts = ( (float) ( ((uint32_t)inMsg.buf[2] << 8) | (uint32_t)inMsg.buf[3]) )  / 1000.0f;
  //     packCurrent = ((float) ( (int16_t) ( ((uint16_t)inMsg.buf[0] << 8) | (uint16_t)inMsg.buf[1]) ) )  / 100.0f;
  //     packPower = packVolts*packCurrent;
  //     externalPower = packPower + power; //Pack power is positive when charging, vesc power is positive when discharging
  //     uint16_t balances = ((uint16_t)inMsg.buf[4])<<8 | (uint16_t)inMsg.buf[5];
  //     balancing  = inMsg.buf[4] | inMsg.buf[5];

  //     dischargeInhibited =  inMsg.buf[6];
  //     chargeInhibited =  inMsg.buf[7];

  //     uint16_t masker = 1;
  //     for(int i = 0 ; i < 15 ; i++){
  //       cellBalancing[i] = balances & masker;
  //       masker = masker << 1;
  //     }

  //     if (canDebugSerial) {
  //       Serial.print("0x20 ");
  //       Serial.print(inMsg.buf[0], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[1], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[2], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[3], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[4], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[5], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[6], HEX); Serial.print(' ');
  //       Serial.print(inMsg.buf[7], HEX); Serial.print(' ');
  //       Serial.print(canTestVal); Serial.print(' ');
  //       Serial.println(balancing);
  //     }
  //   }
  //   else if (inMsg.id == 0x22){ //Cell volts 1-5
  //     bmsCells1_5Updated = true;
  //     cellVolts[0] = ((uint32_t)inMsg.buf[1] >> 4) | ((uint32_t)inMsg.buf[0] << 4); cellVogelts[0] += 405;
  //     cellVolts[1] = ((uint32_t)(inMsg.buf[1] & 0x0F) << 8) | ((uint32_t)inMsg.buf[2]); cellVolts[1] += 405;
  //     cellVolts[2] = ((uint32_t)inMsg.buf[4] >> 4) | ((uint32_t)inMsg.buf[3] << 4); cellVolts[2] += 405;
  //     cellVolts[3] = ((uint32_t)(inMsg.buf[4] & 0x0F) << 8) | ((uint32_t)inMsg.buf[5]); cellVolts[3] += 405;
  //     cellVolts[4] = ((uint32_t)inMsg.buf[7] >> 4) | ((uint32_t)inMsg.buf[6] << 4); cellVolts[4] += 405;
  //   }
  //   else if (inMsg.id == 0x26){ //Cell volts 6-10
  //     bmsCells6_10Updated = true;
  //     cellVolts[5] = ((uint32_t)inMsg.buf[1] >> 4) | ((uint32_t)inMsg.buf[0] << 4); cellVolts[5] += 405;
  //     cellVolts[6] = ((uint32_t)(inMsg.buf[1] & 0x0F) << 8) | ((uint32_t)inMsg.buf[2]); cellVolts[6] += 405;
  //     cellVolts[7] = ((uint32_t)inMsg.buf[4] >> 4) | ((uint32_t)inMsg.buf[3] << 4); cellVolts[7] += 405;
  //     cellVolts[8] = ((uint32_t)(inMsg.buf[4] & 0x0F) << 8) | ((uint32_t)inMsg.buf[5]); cellVolts[8] += 405;
  //     cellVolts[9] = ((uint32_t)inMsg.buf[7] >> 4) | ((uint32_t)inMsg.buf[6] << 4); cellVolts[9] += 405;
  //   }
  //   else if (inMsg.id == 0x30){ //Cell volts 11-15
  //     bmsCells11_15Updated = true;
  //     cellVolts[10] = ((uint32_t)inMsg.buf[1] >> 4) | ((uint32_t)inMsg.buf[0] << 4); cellVolts[10] += 405;
  //     cellVolts[11] = ((uint32_t)(inMsg.buf[1] & 0x0F) << 8) | ((uint32_t)inMsg.buf[2]); cellVolts[11] += 405;
  //     cellVolts[12] = ((uint32_t)inMsg.buf[4] >> 4) | ((uint32_t)inMsg.buf[3] << 4); cellVolts[12] += 405;
  //     cellVolts[13] = ((uint32_t)(inMsg.buf[4] & 0x0F) << 8) | ((uint32_t)inMsg.buf[5]); cellVolts[13] += 405;
  //     cellVolts[14] = ((uint32_t)inMsg.buf[7] >> 4) | ((uint32_t)inMsg.buf[6] << 4); cellVolts[14] += 405;
  //   }
  //   else if ( inMsg.id == 0x46) { //Cell extremes
  //     bmsCellExtremesUpdated = true;
  //     lowCell = (int32_t)( ((uint32_t)inMsg.buf[4] << 8) | (uint32_t)inMsg.buf[5] );
  //     highCell = (int32_t)( ((uint32_t)inMsg.buf[1] << 8) | (uint32_t)inMsg.buf[2] );
  //     cellDiff = (int32_t)( ((uint32_t)inMsg.buf[6] << 8) | (uint32_t)inMsg.buf[7] );
  //     if (canDebugSerial) {
  //       Serial.print("0x46 ");
  //       Serial.print(lowCell); Serial.print(' ');
  //       Serial.print(highCell); Serial.print(' ');
  //       Serial.println(cellDiff);
  //     }
  //   }
  //   else if ( inMsg.id == 0x137){  //Runtime and info
      
  //   }
  //   else if ( inMsg.id == 0x40) { //Temperatures
  //     bmsTemperatureUpdated = true;
  //     bmsExternalTemp = ( (float)( ((uint32_t)inMsg.buf[0] << 8) | (uint32_t)inMsg.buf[1] ) ) / 10.0;
  //     bmsTemp = ( (float)( ((uint32_t)inMsg.buf[2] << 8) | (uint32_t)inMsg.buf[3] ) ) / 10.0;
  //     bmsChipTempHi = ( (float)( ((uint32_t)inMsg.buf[4] << 8) | (uint32_t)inMsg.buf[5] ) ) / 10.0;
  //     bmsChipTempLo = ( (float)( ((uint32_t)inMsg.buf[6] << 8) | (uint32_t)inMsg.buf[7] ) ) / 10.0;


  //     if (bmsExternalTemp > hotTempLim || bmsTemp > hotTempLim || bmsChipTempHi > hotTempLim) {
  //       bmsHot = true;
  //     }
  //     else {
  //       bmsHot = false;
  //     }
  //     if (canDebugSerial) {
  //       Serial.print("0x40 ");
  //       Serial.print(bmsExternalTemp); Serial.print(' ');
  //       Serial.print(bmsTemp); Serial.print(' ');
  //       Serial.print(bmsChipTempHi); Serial.print(' ');
  //       Serial.println(bmsChipTempLo);
  //     }
  //   }
  //   else if ( inMsg.id == 0x43){  //Energy
  //     bmsEnergyUpdated = true;
  //     packEnergy = (int64_t)( ((uint64_t)inMsg.buf[0] << 56) | ((uint64_t)inMsg.buf[1] << 48) | ((uint64_t)inMsg.buf[2] << 40) | ((uint64_t)inMsg.buf[3] << 32) |
  //     ((uint64_t)inMsg.buf[4] << 24) | ((uint64_t)inMsg.buf[5] << 16) | ((uint64_t)inMsg.buf[6] << 8) | ((uint64_t)inMsg.buf[0]) );
      
  //   }
  //   else {
  //     if (canDebugSerial) {
  //       //Serial.println(NUM_MAILBOXES);
  //     }
  //   }
  //   //canTestVal = 200;
  // }
}

CAN_message_t FlexCANify(can_frame victim){ //Makes a FlexCAN message from the library's can message type

  CAN_message_t reformed;
  reformed.flags.extended=1;
  reformed.id = victim.can_id;
  reformed.len = victim.can_dlc;
  for(int i = 0 ; i<8 ; i++){
    reformed.buf[i] = victim.data[i];
  }
  return reformed;
}

can_frame VescCANify(CAN_message_t victim){ //Makes a can message of the library's type from a FlexCAN message

  can_frame reformed;
  reformed.can_id = victim.id;
  reformed.can_dlc = victim.len;
  for(int i = 0 ; i<8 ; i++){
    reformed.data[i] = victim.buf[i];
  }
  return reformed;
}

void KickDog() {
  static unsigned int dogKickStamp = 0;
  //Serial.println("Kicking the dog!");
  if (millis() - dogKickStamp > 220) {
    dogKickStamp = millis();
    noInterrupts();
    WDOG_REFRESH = 0xA602;
    WDOG_REFRESH = 0xB480;
    interrupts();
  }
}

void printResetType() {
  if (RCM_SRS1 & RCM_SRS1_SACKERR)   Serial.println("[RCM_SRS1] - Stop Mode Acknowledge Error Reset");
  if (RCM_SRS1 & RCM_SRS1_MDM_AP)    Serial.println("[RCM_SRS1] - MDM-AP Reset");
  if (RCM_SRS1 & RCM_SRS1_SW)        Serial.println("[RCM_SRS1] - Software Reset");
  if (RCM_SRS1 & RCM_SRS1_LOCKUP)    Serial.println("[RCM_SRS1] - Core Lockup Event Reset");
  if (RCM_SRS0 & RCM_SRS0_POR)       Serial.println("[RCM_SRS0] - Power-on Reset");
  if (RCM_SRS0 & RCM_SRS0_PIN)       Serial.println("[RCM_SRS0] - External Pin Reset");
  if (RCM_SRS0 & RCM_SRS0_WDOG)      Serial.println("[RCM_SRS0] - Watchdog(COP) Reset");
  if (RCM_SRS0 & RCM_SRS0_LOC)       Serial.println("[RCM_SRS0] - Loss of External Clock Reset");
  if (RCM_SRS0 & RCM_SRS0_LOL)       Serial.println("[RCM_SRS0] - Loss of Lock in PLL Reset");
  if (RCM_SRS0 & RCM_SRS0_LVD)       Serial.println("[RCM_SRS0] - Low-voltage Detect Reset");
}

uint8_t returnResetType() {
  if (RCM_SRS1 & RCM_SRS1_SACKERR)   return 1; //("[RCM_SRS1] - Stop Mode Acknowledge Error Reset");
  if (RCM_SRS1 & RCM_SRS1_MDM_AP)    return 2;//("[RCM_SRS1] - MDM-AP Reset");
  if (RCM_SRS1 & RCM_SRS1_SW)        return 3;//("[RCM_SRS1] - Software Reset");
  if (RCM_SRS1 & RCM_SRS1_LOCKUP)    return 4;//("[RCM_SRS1] - Core Lockup Event Reset");
  if (RCM_SRS0 & RCM_SRS0_POR)       return 5;//("[RCM_SRS0] - Power-on Reset");
  if (RCM_SRS0 & RCM_SRS0_PIN)       return 6;//("[RCM_SRS0] - External Pin Reset");
  if (RCM_SRS0 & RCM_SRS0_WDOG)      return 7;//("[RCM_SRS0] - Watchdog(COP) Reset");
  if (RCM_SRS0 & RCM_SRS0_LOC)       return 8;//("[RCM_SRS0] - Loss of External Clock Reset");
  if (RCM_SRS0 & RCM_SRS0_LOL)       return 9;//("[RCM_SRS0] - Loss of Lock in PLL Reset");
  if (RCM_SRS0 & RCM_SRS0_LVD)       return 10;//("[RCM_SRS0] - Low-voltage Detect Reset");
  return 0;
}

#ifdef __cplusplus
extern "C" {
void startup_early_hook();
}
extern "C" {
#endif
  void startup_early_hook() {
    WDOG_TOVALL = (500); // The next 2 lines sets the time-out value. This is the value that the watchdog timer compare itself to.
    WDOG_TOVALH = 0;
    WDOG_STCTRLH = (WDOG_STCTRLH_ALLOWUPDATE | WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN | WDOG_STCTRLH_STOPEN); // Enable WDG
    //WDOG_PRESC = 0; // prescaler
  }
#ifdef __cplusplus
}
#endif
